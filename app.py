from flask import Flask, request,jsonify,Response
from pymongo import MongoClient
import json
from bson import json_util


app = Flask(__name__)

client = MongoClient('mongodb://localhost:27017')
db = client["entity"]
col = db["entity_type"]

@app.route("/entitytype/v1/")
def entitytypeapi():
        cursor = col.find()
        l = []
        for document in cursor:
            l.append(document)
        data_dict = {'error':'103','error_msg': 'null','data': l}
        ff = json_util.dumps(data_dict)

        return Response(ff)

@app.route("/entitytype/v2/<n>")
def entitytypename1(n):
        nn = n
        c = col.find_one({'name': str(nn)})
        print(c)
        if c:
            data_dict = {'error':'103','error_msg': 'null','data': c}

        else:
            data_dict = {'error': '101', 'error_msg': 'null'}
        ff = json_util.dumps(data_dict)
        return Response(ff)

@app.route('/entitytype/v3/',methods=['POST'])
def insert():
    # print(request)
    # print(request.json, type(request.json))
    if request.method == "POST":
        data = {'name': request.json['name'], 'description': request.json['description']}
        print(data)
        col.insert_one(data)
        # return Response(jsonify(json.loads(json_util.dumps(d))))
        ff = json.loads(json_util.dumps(data))
        return jsonify(ff)


@app.route('/delete_no_item/', methods=['DELETE'])
def delete_item_no_item_found():
    return  Response('106: entity_type_name can’t be null, please enter a sting value')

@app.route('/delete/<goal_name>', methods=['DELETE'])
def delete_item(goal_name):
    name=goal_name
    if col.find_one({'name': name}) == None:
         return "101: invalid entity_type_name"
    else:
        col.delete_one({'name': name})
        return "deleted successfully!"

if __name__ == "__main__":
    app.run(debug=True)
import unittest
import requests
import json
# import app
# import urllib
# from flask import request
# from app import entitytypeapi




class TestCalc(unittest.TestCase):

    def test_entitytypeapi(self):
        response = requests.get(' http://127.0.0.1:5000/entitytype/v1/')
        self.assertEqual(response.status_code, 200)


    def test_entitytypename1(self):
        response = requests.get(' http://127.0.0.1:5000/entitytype/v1?name=parth')
        self.assertEqual(response.status_code, 200)

    def test_insert(self):
        header = {
            "Content-Type": "application/json"
        }
        data = {
            "name": "parth",
            "description": "hello"
        }
        import json
        response = requests.post(' http://127.0.0.1:5000/entitytype/v3/', data=json.dumps(data), headers=header)
        self.assertEqual(response.status_code, 200)

    def test_delete_item_no_item_found(self):
        header = {
                    "Content-Type": "application/json"
                }
        data = {"name":""}
        import json
        response = requests.delete(' http://127.0.0.1:5000/delete_no_item/', data=json.dumps(data), headers=header)
        self.assertEqual(response.status_code, 200)

    def test_delete(self):
        header = {
                    "Content-Type": "application/json"
                }
        data = {"name":"parth"}
        import json
        response = requests.delete(' http://127.0.0.1:5000/delete/<goal_name>', data=json.dumps(data), headers=header)
        self.assertEqual(response.status_code, 200)







if __name__ == '__main__':
        unittest.main()